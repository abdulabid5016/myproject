package com.luv2code.springdemo;

public class CricketCoach implements Coach {
private FortuneService fortuneService;
private String emailAddress;
private String team;

public String getEmailAddress() {
	return emailAddress;
}

public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
}

public String getTeam() {
	return team;
}

public void setTeam(String team) {
	this.team = team;
}

public CricketCoach() {
	System.out.println("no-arg construtctor");
}

	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("with -arg construtctor");

		this.fortuneService = fortuneService;
	
}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "pratice for 3 hours";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return "Today is ur lucky day";
	}

}
